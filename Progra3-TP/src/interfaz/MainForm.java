package interfaz;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.util.ArrayList;
import java.awt.Color;
import javax.swing.border.MatteBorder;
import negocio.Tablero;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class MainForm {

	private static JFrame juego;
	private static JFrame menu;
		
	public Tablero tab;
	private static int tamañoMatriz = 0;
	public static MainForm window;
	
	private static boolean tamañoSeleccionado = false;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
						window = new MainForm();
						
						mostrarMenuPrincipal();
						MainForm.menu.setVisible(true);
						
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainForm() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (tamañoSeleccionado == true) 
		iniciarTablero();
		

	}

	
	
	private static void mostrarMenuPrincipal() {
		//MENU PANTALLA INICIAL
		menu = new JFrame();
		menu.setBounds(00, 00, 600, 400);
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.getContentPane().setLayout(null);
		
		JLabel tituloJuego = new JLabel("Lights out");
		tituloJuego.setFont(new Font("Tahoma", Font.PLAIN, 38));
		tituloJuego.setBounds(216, 27, 183, 46);
		menu.getContentPane().add(tituloJuego);
		
		JButton botonFacil = new JButton("F\u00C1CIL");
		botonFacil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccionarDificultad(4);
			}
		});
		botonFacil.setBounds(259, 169, 85, 21);
		menu.getContentPane().add(botonFacil);
		
		JButton botonIntermedio = new JButton("MEDIO");
		botonIntermedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccionarDificultad(5);					
			}
		});
		botonIntermedio.setBounds(259, 225, 85, 21);
		menu.getContentPane().add(botonIntermedio);
		
		JButton botonDificil = new JButton("DIF\u00CDCL");
		botonDificil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccionarDificultad(6);
			}
		});
		
		botonDificil.setBounds(259, 280, 85, 21);
		menu.getContentPane().add(botonDificil);
		
		JLabel tituloJuego_1 = new JLabel("Seleccionar Dificultad");
		tituloJuego_1.setHorizontalAlignment(SwingConstants.CENTER);
		tituloJuego_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tituloJuego_1.setBounds(205, 112, 183, 46);
		menu.getContentPane().add(tituloJuego_1);
				
	}

	
	private void iniciarTablero() {
				
		//JUEGO PRINCIPAL
		juego = new JFrame();
		juego.setBounds(00, 00, getTamañoMatriz() * 60 + 140, getTamañoMatriz() * 60 + 140);
		juego.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		juego.getContentPane().setLayout(null);

		//Arreglo de todos los botones
		ArrayList<JButton> arregloBotones = new ArrayList<JButton>();

		//Instancia el tablero
		tab = new Tablero(getTamañoMatriz());
		
		//Contador de movimientos
		JLabel movimientos = new JLabel("Movimientos = " + tab.getMovimientos());
		movimientos.setBounds(10, 11, 90, 14);
		juego.getContentPane().add(movimientos);
		
		//Movimientos minimos para ganar
		JLabel movimientosganar = new JLabel("Movimientos minimos posibles = " + tab.getTamano());
		movimientosganar.setBounds(10, 21, 200, 14);
		juego.getContentPane().add(movimientosganar);
		
		//Record
		JLabel record = new JLabel("Record= " + tab.getRecord());
		record.setBounds(10, 31, 200, 14);
		juego.getContentPane().add(record);
		
//------------------------------------------------------------------interfaz de botones-------------------------------------------------------------//

		for (int fila = 0; fila < MainForm.getTamañoMatriz(); fila++) {
			for (int columna = 0; columna < MainForm.getTamañoMatriz(); columna++) {
				
				JButton boton = new JButton("");
				final int filaFinal = fila;
				final int columnaFinal = columna;
				boton.setBounds(60 * (fila + 1), 60 * (columna + 1), 50, 50);
				boton.setBorder(new MatteBorder(0, 0, 0, 0, (Color) new Color(0, 0, 0)));
				
				juego.getContentPane().add(boton);
				arregloBotones.add(boton);

				boton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						tab.cambiarEstado(filaFinal, columnaFinal);
						movimientos.setText("Movimientos = " + tab.getMovimientos());

						actualizarTablero(arregloBotones);
					}});
			}
		}

		// Actualiza el tablero
		actualizarTablero(arregloBotones);
	}
//-----------------------------------------------------------------------------------------------------------------------------------------------------//
	

	
	protected void actualizarTablero(ArrayList<JButton> botones) {
		int indice = 0;

		for (int i = 0; i < tab.matriz.length; i++) {
			for (int j = 0; j < tab.matriz[i].length; j++) {
				indice++;
				if (tab.matriz[i][j]) {
					botones.get(indice - 1).setBorder(new MatteBorder(0, 0, 0, 0, (Color) new Color(0, 0, 0)));
					botones.get(indice - 1).setBackground(Color.black);
					botones.get(indice - 1).setIcon(new ImageIcon("Imagenes/light on.jpg"));
					
				} else {
					botones.get(indice - 1).setBorderPainted(true);
					botones.get(indice - 1).setBackground(Color.BLACK);
					botones.get(indice - 1).setIcon(new ImageIcon("Imagenes/light off.jpg"));
				}
			}
		}
		if (tab.gameOver()) {
			
			//-----------------------mensaje record-------------------------------//
			if(tab.getMovimientos()<tab.getRecord()) {
				tab.setRecord(tab.getMovimientos());	
				
				JOptionPane.showConfirmDialog(juego, "Felicitaciones, ganaste el juego!\n"+"Lograste Nuevo Record: "+tab.getRecord(),"Lights out", 2);
			}
			//-----------------------mensaje record-------------------------------//
			else JOptionPane.showConfirmDialog(juego, "Felicitaciones, ganaste el juego!\n"+"Record Anterior: "+tab.getRecord(),"Lights out", 2);
			
			tab.setMovimientos(0);
			MainForm.juego.setVisible(false);
			MainForm.menu.setVisible(true);
		} 
	}
	
	private static void seleccionarDificultad(int nivel){
		setTamañoMatriz(nivel);
		tamañoSeleccionado = true;
		new MainForm();
		juego.setVisible(true);
		MainForm.menu.setVisible(false);
	}
	

	public static int getTamañoMatriz() {
		return tamañoMatriz;
	}

	public static void setTamañoMatriz(int tamañoMatriz) {
		MainForm.tamañoMatriz = tamañoMatriz;
	}
}
