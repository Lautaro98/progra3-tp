package negocio;


public class Tablero {
	public int tama�o;
	public int movimientos;
	public int recordFacil = 999;
	public static int recordMedio = 999;
	public static int recordDificil = 999;
	
	public boolean[][] matriz;
	
	

	public Tablero(int tamano) {
		this.tama�o=tamano;
		matriz = new boolean[tamano][tamano];
		// iniciar con random el tablero...
		for (int i = 0; i < tamano; i++) {
			 int rand1 = (int) Math.floor(Math.random()*tamano);
			 int rand2 = (int) Math.floor(Math.random()*tamano); 
			 this.cambiarEstado(rand1, rand2);
		}
		
		movimientos = 0;
	}

	public void cambiarEstado(int fila, int columna) {
		matriz[fila][columna] = !matriz[fila][columna];

		this.movimientos++;
		try {
			matriz[fila][columna - 1] = !matriz[fila][columna - 1];
		} catch (Exception e) {

		}
		try {
			matriz[fila][columna + 1] = !matriz[fila][columna + 1];
		} catch (Exception e) {

		}
		try {
			matriz[fila + 1][columna] = !matriz[fila + 1][columna];
		} catch (Exception e) {

		}
		try {
			matriz[fila - 1][columna] = !matriz[fila - 1][columna];

		} catch (Exception e) {

		}
	}

	public boolean gameOver() {
		boolean flag = true;

		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				flag = flag && (matriz[i][j] == false);
			}
		}
		return flag;
	}

	public int getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(int movimientos) {
		this.movimientos = movimientos;
	}

	public int getTamano() {
		return tama�o;
	}

	public void setTamano(int tama�o) {
		this.tama�o = tama�o;
	}

	public int getRecord() {
		return recordFacil;
	}

	public void setRecord(int record) {
		this.recordFacil = record;
	}
	
	

}